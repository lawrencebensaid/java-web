package model;
//loginBean with getters and setters to acces data
public class LoginBean {

	private String username;
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String newPassword) {
		password = newPassword;
	}

	public String getName() {
		return username;
	}

	public void setName(String newUsername) {
		username = newUsername;
	}
}
