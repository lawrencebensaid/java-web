package controller;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.PostBean;
import model.UserBean;
import util.PostDao;

@WebServlet("/blog")
public class BlogPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = Logger.getLogger(BlogPostServlet.class.getName());
	//Get all get requests
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserBean userBean = (UserBean) request.getSession().getAttribute("userBean");
		//Check if user exists else redirect to login
		if (userBean != null) {
			LOGGER.log(Level.INFO, "Blog servlet requested (get).");
			//create list op posts
			List<PostBean> list;
			try {
			//get list from database
				list = PostDao.getPost();
				request.setAttribute("listPost", list);
			} catch (Exception e) {
				System.out.println("Error while getting postlist: " + e.getMessage());
			}
			// Forward the request
			System.out.println("before request");
			request.getRequestDispatcher("/WEB-INF/post.jsp").forward(request, response);
			System.out.println("after request");
			// doPost(request, response);
		} else {
			request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.log(Level.INFO, "Blog servlet requested (post).");
	}

}
