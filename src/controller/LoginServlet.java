package controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.LoginBean;
import model.UserBean;
import util.LoginDao;

/**
 * Servlet implementation class Servlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = Logger.getLogger(LoginServlet.class.getName());

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.log(Level.INFO, "Login servlet requested (get).");

		// Forward to the login form with an empty loginBean object.
		request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.log(Level.INFO, "Admin servlet requested (post).");
		//Create loginBean
		LoginBean loginBean = new LoginBean();
		//Set login parameters
		loginBean.setName(request.getParameter("name"));
		loginBean.setPassword(request.getParameter("password"));
		try {
			// Create LoginDao object and validatie user information
			UserBean userBean = LoginDao.authenticateUser(loginBean);
			// Store userBean in session
			HttpSession session = ((HttpServletRequest) request).getSession(true);
			session.setAttribute("userBean", userBean);
			// Go to the homepage
			response.sendRedirect("index.jsp");

		} catch (Exception e) {
			// Store errorMessage in the session
			request.setAttribute("errorMessage", "Email or password unknown - please try again.");

			// Store loginBean in request
			request.setAttribute("loginBean", loginBean);

			// Redirect to form again.
			request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
		}
	}

}
