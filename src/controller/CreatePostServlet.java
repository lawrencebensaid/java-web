package controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.PostBean;
import model.UserBean;
import util.CreatePostDao;

/**
 * Servlet implementation class CreatePostServlet
 */
@WebServlet("/create")
public class CreatePostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CreatePostServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserBean userBean = (UserBean) request.getSession().getAttribute("userBean");
		//check if user exists else redirect to login
		if (userBean != null) {
			request.getRequestDispatcher("WEB-INF/create.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserBean userBean = (UserBean) request.getSession().getAttribute("userBean");
		PostBean postBean = new PostBean();
		//check if userbean is not empty
		if (userBean != null) {
			try {
				//create local time variable
				LocalDateTime currentDateTime = LocalDateTime.now();
				DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE;
				String formattedDateTime = currentDateTime.format(formatter);
				//set post data
				postBean.setDate(formattedDateTime);
				postBean.setTitle(request.getParameter("title"));
				postBean.setContent(request.getParameter("content"));
				postBean.setUser(userBean.getUserName());
				// Create LoginDao object and validatie user information
				CreatePostDao.createPost(postBean);
				//send response to the view
				response.sendRedirect("./blog");
			} catch (Exception e) {
				// Store errorMessage in the session
				request.setAttribute("errorMessage", "Failed to post");
			
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
		} else {
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
	}

}
