package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.UserBean;
import util.UpdateDao;

@WebServlet("/account")
public class AccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AccountServlet() {
		super();
	}
	// get all get requests
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserBean userBean = (UserBean) request.getSession().getAttribute("userBean");
		//Check if user is logged in else go to login
		if (userBean != null) {
		request.getRequestDispatcher("./WEB-INF/account.jsp").forward(request, response);
		} else {
		request.getRequestDispatcher("./WEB-INF/login.jsp").forward(request, response);
		}
	}
	//get all POST requests
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserBean userBean1 = new UserBean();
		UserBean userBean = (UserBean) request.getSession().getAttribute("userBean");
		//Check if user is logged in else go to login
		if (userBean != null) {
		try {
		//set userBean data + update session userbean
			userBean1.setID(userBean.getID());
			userBean1.setFirstName(request.getParameter("firstname"));
			userBean1.setLastName(request.getParameter("lastname"));
			userBean1.setUserName(request.getParameter("username"));

			userBean.setFirstName(request.getParameter("firstname"));
			userBean.setLastName(request.getParameter("lastname"));
			userBean.setUserName(request.getParameter("username"));

			// Create LoginDao object and validatie user information
			UpdateDao.updateUser(userBean);
			response.sendRedirect("./account");
		} catch (Exception e) {
			// Store errorMessage in the session
			request.setAttribute("errorMessage", "Failed to update");
			// Let user login again
			request.getRequestDispatcher("./account").forward(request, response);
		}
		} else {
			request.getRequestDispatcher("./WEB-INF/login.jsp").forward(request, response);
		}
	}

}
