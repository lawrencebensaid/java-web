package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.DeleteDao;

@WebServlet("/delete")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public DeleteServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//get userID
		String userID;
		try {
			//Delete te user with de DeleteDao
			userID = request.getParameter("id");
			DeleteDao.deleteUser(userID);
			//redirect to admin
			response.sendRedirect("./admin");
		} catch (Exception e) {
			request.getRequestDispatcher("/WEB-INF/admin.jsp").forward(request, response);
		}
	}

}
