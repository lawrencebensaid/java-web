package controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.UserBean;
import util.RegisterDao;

/**
 * Servlet implementation class Servlet
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = Logger.getLogger(LoginServlet.class.getName());

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.log(Level.INFO, "Register servlet requested (get).");

		// Forward to the login form with an empty loginBean object.
		request.getRequestDispatcher("WEB-INF/register.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.log(Level.INFO, "Register servlet requested (post).");
		//create userBean to register
		UserBean userBean = new UserBean();
		try {
			//add data from POST to userBean
			userBean.setFirstName(request.getParameter("firstname"));
			userBean.setLastName(request.getParameter("lastname"));
			userBean.setUserName(request.getParameter("username"));
			userBean.setPassword(request.getParameter("password"));
			// Create LoginDao object and validatie user information
			RegisterDao.registerUser(userBean);
			//Redirect to login page
			response.sendRedirect("./login");
		} catch (Exception e) {
			// Store errorMessage in the session
			request.setAttribute("errorMessage", "Failed to register");

			request.getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
		}
	}

}
