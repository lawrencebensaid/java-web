package controller;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.UserBean;
import util.UserDao;

@WebServlet("/admin")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = Logger.getLogger(AdminServlet.class.getName());
	//check for all get requests
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		UserBean userBean = (UserBean) request.getSession().getAttribute("userBean");
		//Check if user is logged in else redirect back
		if (userBean == null) {
			request.getRequestDispatcher("/index.jsp").forward(request, response);
			return;
		}//check if user is admin
		if (userBean.getRole() == 2) {
			LOGGER.log(Level.INFO, "Admin servlet requested (get).");
			// Get list of users
			List<UserBean> list;
			try {
				list = UserDao.getUsers();
				request.setAttribute("listUsers", list);
			} catch (Exception e) {
				System.out.println("Error while getting userlist: " + e.getMessage());
			}
			// Forward the request
			request.getRequestDispatcher("./WEB-INF/admin.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.log(Level.INFO, "Admin servlet requested (post).");

	}

}
