package util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
//Class to make database connection
public class DBConnection {

	private static Logger LOGGER = Logger.getLogger(DBConnection.class.getName());
	private static DataSource dataSource;

	public static Connection getConnection() {
		//set connection
		Connection connection = null;
		
		if (dataSource == null) {
			try {
			//initialize connection
				Context initContext = new InitialContext();
				dataSource = (DataSource) initContext.lookup("java:comp/env/javaweb");
			} catch (NamingException e) {
				LOGGER.log(Level.WARNING, "Error while retrieving application context: " + e.getMessage());
			}
		}

		try {
			connection = dataSource.getConnection();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Error while connecting to database: " + e.getMessage());
		}

		return connection;

	}
}