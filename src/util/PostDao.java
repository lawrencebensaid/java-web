package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.PostBean;

public class PostDao {
	private static Logger LOGGER = Logger.getLogger(PostDao.class.getName());

	public static ArrayList<PostBean> getPost() throws Exception {
	//create list for the posts
		ArrayList<PostBean> list = new ArrayList<PostBean>();
		try {
			//make database connectio
			Connection connection = DBConnection.getConnection();
			//create query
			String query = "SELECT * FROM posts LIMIT 0,100";
			//set te statement
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			ResultSet resultSet = preparedStatement.executeQuery();
			//loop threw results and add them to the arraylist
			while (resultSet.next()) {
				PostBean postBean = new PostBean();
				postBean.setTitle(resultSet.getString("title"));
				postBean.setContent(resultSet.getString("content"));
				postBean.setUser(resultSet.getString("user"));
				postBean.setDate(resultSet.getString("postdate"));
				list.add(postBean);
			}
			LOGGER.log(Level.INFO, "Retrieved " + list.size() + " posts from database.");
			return list;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Error while retrieving " + " posts: " + e.getMessage());
		}
		throw new Exception("Error while retrieving posts.");
	}
}