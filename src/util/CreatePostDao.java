package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.PostBean;
//Dao to create a post
public class CreatePostDao {
	private static Logger LOGGER = Logger.getLogger(CreatePostDao.class.getName());

	public static void createPost(PostBean postBean) throws Exception {
		try {			
			Connection connection = DBConnection.getConnection();
			//query for the server
			String query = "INSERT INTO posts(title, content, user, postdate)" + " values (?, ?, ?, ?)";
			// set all the preparedstatement parameters
			PreparedStatement st = connection.prepareStatement(query);
			//add data from variable to query
			st.setString(1, postBean.getTitle());
			st.setString(2, postBean.getContent());
			st.setString(3, postBean.getUser());
			st.setString(4, postBean.getDate());

			// execute the preparedstatement insert
			st.executeUpdate();
			st.close();
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Error while creating post in" + e.getMessage());
		}
	}
}
