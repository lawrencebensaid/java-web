package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.UserBean;
//Dao to get all users
public class UserDao {
	private static Logger LOGGER = Logger.getLogger(UserDao.class.getName());

	public static ArrayList<UserBean> getUsers() throws Exception {
		//create a list to keep all the user models in
		ArrayList<UserBean> list = new ArrayList<UserBean>();
		try {
			//make database connection
			Connection connection = DBConnection.getConnection();
			//make query for the database
			String query = "SELECT * FROM users LIMIT 0,100";
			//add the prepared statement
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			ResultSet resultSet = preparedStatement.executeQuery();
			//loop threw results and att them to the arraylist
			while (resultSet.next()) {
				UserBean userBean = new UserBean();
				userBean.setID(resultSet.getInt("id"));
				userBean.setFirstName(resultSet.getString("Firstname"));
				userBean.setLastName(resultSet.getString("Lastname"));
				userBean.setUserName(resultSet.getString("username"));
				userBean.setLastLogin(resultSet.getString("lastlogin"));
				list.add(userBean);
			}
			LOGGER.log(Level.INFO, "Retrieved " + list.size() + " users from database.");
			return list;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Error while retrieving " + " users: " + e.getMessage());
		}
		throw new Exception("Error while retrieving users.");
	}
}