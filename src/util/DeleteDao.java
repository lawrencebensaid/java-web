package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
//delete data from users
public class DeleteDao {
	private static Logger LOGGER = Logger.getLogger(DeleteDao.class.getName());

	public static void deleteUser(String id) throws Exception {
		try {
			//create connection with class
			Connection connection = DBConnection.getConnection();
			//create query
			String query = "DELETE FROM Users WHERE id= ?";
			// set all the preparedstatement parameters
			PreparedStatement st = connection.prepareStatement(query);
			st.setString(1, id);

			// execute the preparedstatement insert
			st.executeUpdate();
			st.close();
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Error while deleting user" + e.getMessage());
		}
	}
}
