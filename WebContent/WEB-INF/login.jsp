<html>
<head>
<!-- Include head links -->
<%@ include file="jspf/head.content.jspf"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Declare session userBean -->
<jsp:useBean id="userBean" class="model.UserBean" scope="session" />
</head>
<body>
<!-- Site background -->
	<div class="login-background">
		<div class="container container-margin">
		<!-- Login form with all the data to POST loginServlet -->
			<div class="login-form">
				<div class="main-div">
					<div class="panel">
						<h2>Login</h2>
						<p>Please enter your username and password</p>
					</div>
					<form id="Login" action="login" method="POST">

						<div class="form-group">
							<input type="text" name="name" class="form-control"
								id="inputEmail" placeholder="Username">
						</div>

						<div class="form-group">

							<input type="password" name="password" class="form-control"
								id="inputPassword" placeholder="Password">
						</div>
						<button type="submit" class="btn btn-primary">Login</button>

					</form>
				</div>
			</div>
		</div>
	</div>