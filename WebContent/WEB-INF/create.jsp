<html>
<head>
<!-- Include head links -->
<%@ include file="./jspf/head.content.jspf"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Declare session userBean -->
<jsp:useBean id="userBean" class="model.UserBean" scope="session" />
</head>
<body>
	<!-- Include header -->
	<%@ include file="/WEB-INF/jspf/header.content.jspf"%>
	<!-- Blog post title -->
	<div class="container center">
		<h1 class="m-4">Create blog post</h1>
	</div>
	<!-- Form to show user data from userBean -->
	<div class="container">
		<form id="Create" action="create" method="POST" autocomplete="off">
			<div class="form-group">
				<input type="text" name="title" class="form-control" id="title"
					placeholder="Title">
			</div>
			<div class="form-group">
				<textarea rows="4" cols="50" name="content" class="form-control" form="Create" placeholder="Content"></textarea>
			</div>
			<!-- Button to update user data with POST to createServlet -->
			<button type="submit" class="btn btn-primary">Create</button>

		</form>
	</div>
	<!-- Footer -->
	<%@ include file="./jspf/footer.content.jspf"%>
</body>
</html>