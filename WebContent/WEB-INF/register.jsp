<html>
<head>
<!-- Include head links -->
<%@ include file="jspf/head.content.jspf"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Declare session userBean -->
<jsp:useBean id="userBean" class="model.UserBean" scope="session" />
</head>
<body>
<!-- Site background -->
	<div class="login-background">
		<div class="container container-margin">
		<!-- Register form with all the data to POST registerServlet -->
			<div class="login-form">
				<div class="main-div">
					<div class="panel">
						<h2>Register</h2>
						<p>Register to the website</p>
					</div>
					<form id="Register" action="register" method="POST" autocomplete="off">

						<div class="form-group">
							<input type="text" name="username" class="form-control"
								id="username" placeholder="Username" autocomplete="off">
						</div>
						<div class="form-group">
							<input type="text" name="firstname" class="form-control"
								id="firstname" placeholder="First name" autocomplete="off">
						</div>
						<div class="form-group">
							<input type="text" name="lastname" class="form-control"
								id="lastname" placeholder="Last name" autocomplete="off">
						</div>
						<div class="form-group">

							<input type="password" name="password" class="form-control"
								id="inputPassword" placeholder="Password" autocomplete="off">
						</div>
						<button type="submit" class="btn btn-primary">Register</button>

					</form>
				</div>
			</div>
		</div>
	</div>