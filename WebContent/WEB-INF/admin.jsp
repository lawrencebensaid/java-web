<html>
<head>
<!-- Include head links -->
<%@ include file="./jspf/head.content.jspf"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Declare session userBean -->
<jsp:useBean id="userBean" class="model.UserBean" scope="session" />
</head>
<body>
	<!-- Include header -->
	<%@ include file="/WEB-INF/jspf/header.content.jspf"%>
	<!-- Admin panel with user data -->
	<div class="container center">
		<h1 class="m-4">Admin Panel</h1>
	</div>
	<div class="container">
		<table class="table">
		<!-- Table head -->
			<thead class="thead-light">
				<tr>
					<th scope="col">ID</th>
					<th scope="col">Username</th>
					<th scope="col">Firstname</th>
					<th scope="col">Lastname</th>
					<th scope="col">Last login</th>
					<th scope="col">Delete</th>
				</tr>
			</thead>
			<!-- Table body -->
			<tbody>
			<!-- forEach to display all users -->
				<c:forEach var="user" items="${listUsers}">
					<tr>
						<th scope="row"><c:out value="${user.ID}" /></th>
						<td><c:out value="${user.userName}" /></td>
						<td><c:out value="${user.firstName}" /></td>
						<td><c:out value="${user.lastName}" /></td>
						<td><c:out value="${user.lastLogin}" /></td>
						<!-- Delete from to POST to delteServlet -->
						<td><form method="post" id="delete" action="delete">
								<input type="hidden" name="id"
									value="<c:out value="${user.ID}" />"> <input
									class="btn btn-danger" type="submit" value="Delete">
							</form></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<!-- Add user link button -->
	<div class="container center">
		<a href="register" class="m-4 btn btn-info">Add user</a>
	</div>
	<!-- Footer -->
	<%@ include file="./jspf/footer.content.jspf"%>
</body>
</html>