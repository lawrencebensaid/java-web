<html>
<head>
<!-- Include head links -->
<%@ include file="./jspf/head.content.jspf"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Declare session userBean -->
<jsp:useBean id="userBean" class="model.UserBean" scope="session" />
</head>
<body>
	<!-- Include header -->
	<%@ include file="/WEB-INF/jspf/header.content.jspf"%>
	<!-- Show username -->
	<div class="container center">
		<h1 class="m-4">
			Account van
			<%=userBean.getUserName()%></h1>
	</div>
	<!-- Form to show user data from userBean -->
	<div class="container">
		<form id="Login" action="account" method="POST">
			<div class="form-group">
				<input type="text" name="username" class="form-control"
					id="username" placeholder="Username"
					value="<%=userBean.getUserName()%>">
			</div>
			<div class="form-group">
				<input type="text" name="firstname" class="form-control"
					id="firstname" placeholder="First name"
					value="<%=userBean.getFirstName()%>">
			</div>
			<div class="form-group">
				<input type="text" name="lastname" class="form-control"
					id="lastname" placeholder="Last name"
					value="<%=userBean.getLastName()%>">
			</div>
			<!-- Button to update user data with POST to updateServlet -->
			<button type="submit" class="btn btn-primary">Update</button>

		</form>
	</div>
	<!-- Footer -->
	<%@ include file="./jspf/footer.content.jspf"%>
</body>
</html>